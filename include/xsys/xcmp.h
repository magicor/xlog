#ifndef __MACRO_XLOG_CMP_H__
#define __MACRO_XLOG_CMP_H__

/**
 * @brief define compare symbol
 * 
 */
#define SYMBOL_NE ("!=")
#define SYMBOL_EQ ("==")
#define SYMBOL_GT (">")
#define SYMBOL_LT ("<")
#define SYMBOL_GE (">=")
#define SYMBOL_LE ("<=")



#endif // __MACRO_XLOG_CMP_H__